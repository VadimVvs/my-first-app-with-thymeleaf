package com.hol.sweet.sweet.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.hol.sweet.sweet.model.User;
import com.hol.sweet.sweet.service.UserService;

@Controller
public class RegistrationController {

	@Autowired
	private UserService userService;

	@GetMapping("/registration")
	public String registration() {
		//model.addAttribute("user", new User());
		return "registration";
	}

	@PostMapping("/registration")
	public String addUser(User user, Model model) {
		if (!userService.addUser(user)) {
			model.addAttribute("message", "user exists!");
			return"registration";
		}
	
		return "redirect:/login";
	}
	
	
	@GetMapping("/activate/{code}")
	public String activate(Model model, @PathVariable("code") String code) {
		boolean isActive = userService.activateUser(code);
		
		if (isActive) {
			model.addAttribute("message","User successfully activated");
		} else {
			model.addAttribute("message","nima");
		}
		return "login";
	}
}
