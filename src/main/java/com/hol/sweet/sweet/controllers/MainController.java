package com.hol.sweet.sweet.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hol.sweet.sweet.model.Message;
import com.hol.sweet.sweet.model.User;
import com.hol.sweet.sweet.repos.MessageRepository;

@Controller
public class MainController {

	@Value("${upload.path}")
	private String uploadPath;

	@Autowired
	private MessageRepository messageRepository;

	@GetMapping("/")
	public String mains(Model model) {

		model.addAttribute("messages", messageRepository.findAll());
		model.addAttribute("messa", "<div><h1>HTML ROBIT</h1></div>");
		return "main";
	}

	@GetMapping("/home")
	public String homePage(Model model) {

		return "home";
	}

	@GetMapping("/sign")
	public String addModel(Model model) {
		model.addAttribute("message", new Message());
		return "add";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@AuthenticationPrincipal User user, @Valid Message message, BindingResult bindingResult,
			Model model, @RequestParam("file") MultipartFile file) throws IOException {

		message.setAuthor(user);
		if (bindingResult.hasErrors()) {
			Collector<FieldError, ?, Map<String, String>> collector = Collectors
					.toMap(fieldError -> fieldError.getField() + "Error", FieldError::getDefaultMessage);
			Map<String, String> errorMap = bindingResult.getFieldErrors().stream().collect(collector);
			model.mergeAttributes(errorMap);
		} else {

			if (file != null && !file.getOriginalFilename().isEmpty()) {
				File upload = new File(uploadPath);
				if (!upload.exists()) {
					upload.mkdir();
				}
				String uuidFileString = UUID.randomUUID().toString();
				String resultFilenameString = uuidFileString + "." + file.getOriginalFilename();

				file.transferTo(new File(uploadPath + "/" + resultFilenameString));

				message.setFilename(resultFilenameString);
			}
			messageRepository.save(message);
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/filtere", method = RequestMethod.POST)
	public String filter(@RequestParam(name = "filter", required = false) String tag, Model model) {
		Iterable<Message> messages;

		if (tag != null && !tag.isEmpty()) {
			messages = messageRepository.findByTag(tag);
		} else {
			messages = messageRepository.findAll();
		}
		System.out.println("tag = " + tag);
		model.addAttribute("messages", messages);
		return "main";
	}

}