package com.hol.sweet.sweet.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hol.sweet.sweet.model.Role;
import com.hol.sweet.sweet.model.User;
import com.hol.sweet.sweet.service.UserService;

@Controller
@RequestMapping("/user")

public class UserController {


	@Autowired
	private UserService userService;

	@GetMapping("/home")
	public String userList(Model model) {
		model.addAttribute("users", userService.findAll());
		return "userList";
	}
	@PreAuthorize("hasAthority('ADMIN')")
	@GetMapping("/{id}")
	public String userEditForm(@PathVariable(name = "id") Long id, Model model) {
		User user = userService.getUserByIDWithRepo(id);
		model.addAttribute("user", user);
		model.addAttribute("roles", Role.values());
		
		return "userEdit";
	}

	@PreAuthorize("hasAthority('ADMIN')")
	@PostMapping("/update/{id}")
	public String updateUser(@PathVariable("id") Long id, @RequestParam String username,
			@RequestParam Map<String, String> form) {
		userService.saveUser(id,username,form);
		return "redirect:/user/home";
	}
	
	@GetMapping("/profile")
	public String getProfile(Model model, @AuthenticationPrincipal User user) {
		model.addAttribute("username",user.getUsername());
		model.addAttribute("email",user.getEmail());
		return "profile";
	}
	 
	@PostMapping("/profile")
	public String updateProfile(
			@AuthenticationPrincipal User user,
			@RequestParam String password,
			@RequestParam String email
			) {
		userService.updateProfile(user,password,email);
		return "redirect:/user/profile";
	}
}
