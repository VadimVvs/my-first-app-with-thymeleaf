package com.hol.sweet.sweet.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.hol.sweet.sweet.model.Role;
import com.hol.sweet.sweet.model.User;
import com.hol.sweet.sweet.repos.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private MailSender mailSender;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findByUsername(username);
	}

	public boolean addUser(User user) {
		User userFromDBUser = userRepository.findByUsername(user.getUsername());
		if (userFromDBUser != null) {
			return false;
		}
		user.setActive(true);
		user.setRoles(Collections.singleton(Role.USER));
		user.setActivationCode(UUID.randomUUID().toString());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		sendMessage(user);
		return true;
	}

	public boolean activateUser(String code) {
		User user = userRepository.findByActivationCode(code);
		if (user == null) {
			return false;
		}

		user.setActivationCode(null);
		userRepository.save(user);
		return true;
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public void saveUser(Long id, String username, Map<String, String> form) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("invalid user id: " + id));
		user.setUsername(username);
		Set<String> roles = Arrays.stream(Role.values()).map(Role::name).collect(Collectors.toSet());
		user.getRoles().clear();
		for (String key : form.keySet()) {
			if (roles.contains(key)) {
				user.getRoles().add(Role.valueOf(key));
			}
		}
		userRepository.save(user);

	}

	public User getUserByIDWithRepo(Long id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("invalid user id: " + id));
		return user;
	}

	public void updateProfile(User user, String password, String email) {
		String userEmail = user.getEmail();
		boolean isEmailChanged = ((email != null && !email.equals(userEmail))
				|| (userEmail != null && !userEmail.equals(email)));
		if (isEmailChanged) {
			user.setEmail(email);
			if (!StringUtils.isEmpty(email)) {
				user.setActivationCode(UUID.randomUUID().toString());
			}
		}

		if (!StringUtils.isEmpty(password)) {
			user.setPassword(password);
		}

		userRepository.save(user);
		if (isEmailChanged) {
			sendMessage(user);
		}
		
	}

	public void sendMessage(User user) {
		if (!StringUtils.isEmpty(user.getEmail())) {
			String message = String.format("Hello, %s! \n" + "Welcome to Sraka . <a href='http://localhost:8080/activate/%s'>tutь</a> ",
					user.getUsername(), user.getActivationCode());
			mailSender.send(user.getEmail(), "Activation code", message);
		}
	}

}
