package com.hol.sweet.sweet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hol.sweet.sweet.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);

	User findByActivationCode(String code);
}
