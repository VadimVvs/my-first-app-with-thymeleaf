package com.hol.sweet.sweet.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hol.sweet.sweet.model.Message;



public interface MessageRepository extends JpaRepository<Message, Long> {

	List<Message> findByTag(String tag);
}
